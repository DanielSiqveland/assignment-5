-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2017 at 10:58 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skiingnewsiste`
--

-- --------------------------------------------------------

--
-- Table structure for table `represents`
--

CREATE TABLE `represents` (
  `sYear` varchar(4) NOT NULL,
  `uName` varchar(20) NOT NULL,
  `totalDistance` int(11) NOT NULL,
  `cId` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `season`
--

CREATE TABLE `season` (
  `sYear` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skiers`
--

CREATE TABLE `skiers` (
  `uName` varchar(20) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `YOB` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skiingclubs`
--

CREATE TABLE `skiingclubs` (
  `cName` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `county` varchar(20) NOT NULL,
  `id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `represents`
--
ALTER TABLE `represents`
  ADD PRIMARY KEY (`uName`,`sYear`,`cId`),
  ADD KEY `cId` (`cId`),
  ADD KEY `sYear` (`sYear`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`sYear`);

--
-- Indexes for table `skiers`
--
ALTER TABLE `skiers`
  ADD PRIMARY KEY (`uName`);

--
-- Indexes for table `skiingclubs`
--
ALTER TABLE `skiingclubs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `represents`
--
ALTER TABLE `represents`
  ADD CONSTRAINT `represents_ibfk_1` FOREIGN KEY (`uName`) REFERENCES `skiers` (`uName`),
  ADD CONSTRAINT `represents_ibfk_3` FOREIGN KEY (`cId`) REFERENCES `skiingclubs` (`id`),
  ADD CONSTRAINT `represents_ibfk_4` FOREIGN KEY (`sYear`) REFERENCES `season` (`sYear`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
