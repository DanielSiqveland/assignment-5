<?php 
	class Model{
 
     public $dom;
     protected $db;
 
 
     public function __construct(){
         try{
             $this->dom = new DOMDocument();
             $this->dom->load('Model/SkierLogs.xml');
             try {
                 $this->db = new PDO('mysql:host=127.0.0.1; dbname=skiing; charset=utf8','root', '');
                 $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             } catch (Exception $e) {
                 echo "Error ";
                 echo $e->getMessage();
             }
             echo "Error.".'<br>';
         }catch(Exception $e){
             echo $e->getMessage();
         }
     }
	

	 
	 
	 /*Declaring 4 public functions*/
	 function main(){	
			
			$this->season();
			$this->skiers();
			$this->skiingclubs();
			$this->represents();
		}
	 
	 function skiers(){
		 
		 $uName = array(); 
		 $firstName =array();
		 $lastName	= array();
		 $YOB= array(); 
		 
		 $skiers =$this->dom->getElementsByTagName("Skier");
		 $firstName =$this->dom->getElementsByTagName("FirstName");
		 $lastName= $this->dom->getElementsByTagName("LastName");
		 $YOB= $this->dom->getElementsByTagName("YearOfBirth"); 
		 
		 
		 foreach($skiers as $Skier){
			 array_push($uName, $Skier->getAttribute("userName"));
		 }
		 foreach($fname as $fn){
			 array_push($firstName, $fn->nodeValue); 
		 }
		 foreach($lastName as $ln){
			 array_push($lastName,$ln->nodeValue);
		 }
		 foreach($birth as $ye){
			 array_push($YOB, $ye->nodeValue); 
			 
		 }
		 for($i=0; $i<sizeof(YOB); $i++){
			 
			try{
				$input =$this->db->prepare('INSERT INTO Skier(userName,FirstName,LastName,YearOfBirth) Values(:uniqName, :firstname, :lastName,:YOB)');
				$input->bindValue(':uniqName', $skier->uName, PDO::PARAM_STR);
                $input->bindValue(':firstname', $skier->firstName, PDO::PARAM_STR);
                $input->bindValue(':lastName', $skier->lastName, PDO::PARAM_STR);
			    $input->bindValue(':YOB', $skier->YOB, PDO::PARAM_INT);
				$input->execute(); 
				echo "<br>works."; 
			}
			catch(Exception $e){
				echo 'Did not manage to update the skier <br>';
				echo $e->getMessage(),"\n";
			}
			
		 }
	 }
	 //
		function skiingclubs(){
			
			$cid= array(); 
			$cnames= array();
			$ccities = array();
			$counties = array(); 
			
			$club =  $this->dom->getElementsByTagName("Club");
			$cname = $this->dom->getElementsByTagName("Name"); 
			$city =  $this->dom->getElementsByTagName("City");
			$county= $this->dom->getElementsByTagName("County");
			
			foreach($clubs as $cl){
				array_push($id, $cl->getAttribute("id"));
				}
				
			foreach($name as $cln){
				array_push($names, $cln->textContent);
				
			}
			foreach($city as $cn){
				array_push($ccities, $cn->textContent);
			}
			foreach($county as $cou){
				array_push($counties, $cou->textContent);
			}
			
			for($i=0; $i<sizeof($id); $i++){
				try{
					
					$input= $this->db->prepare('INSERT INTO Skiingclubs(Clubid,Name,City,County) Values(:cid, :name, :city, :county)');
					$input->$bindValue('clname', $Skiingclubs->cName[$i], PDO::PARAM_STR);
					$input->$bindValue('cname', $Skiingclubs->city[$i], PDO::PARAM_STR);
					$input->$bindValue('couname', $Skiingclubs->county[$i], PDO::PARAM_STR);
					$input->$bindValue('cid', $Skiingclubs->id[$i], PDO::PARAM_STR);
					$input->execute(); 
				echo "<br>works.";
				}
				  catch(Exception $e){
					
					echo 'Did not manage to update the skiingclub <br>';
					echo $e->getMessage(),"\n";
			}	
			
					
					
				}
			}
			
	
			
			
		
		  /* getting fallyears into db  */
		 function season(){
			$season = array(); 
			$seasons =$this->dom->getElementsByTagName('Season');
			foreach(seasons as $se){
				array_push($season, $se->getAttribute("fallYear"));
			}
			
			for($i =0; $i<sizeof($season); $i++){
				try{
					$input=$this->db->prepare('INSERT INTO season(fallYear) Values(:year)');
					$input->bindValue(':year', $season[$i], PDO::PARAM_STR);
					$input->execute(); 
					echo'works';
					
					
				}catch(Exception $e){
					echo"<br>did not update season data";
				    echo $e->getMessage(); "\n";
				}
			}
			
		}
		 function represents(){
 
         $clubId= array();
		 $userNames = array();
         $userSeason16 = array();
         $dist_15 = array();
		 $dist_16 = array();
			
			
		 $xpath = new DOMXPath($this->dom);
			//skiers of 2015 season 
		 $usNAme15= $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
		 
		 foreach($usNAme15 as $user){
			 array_push($userNames, trim($user->textContent));
		 }
		 
		 //tot dist of each skier
		 
		 $sum =0; 
		 for($i=0; $i<sizeof(userNames); $i++){
			 $distance= $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$userNames[$i].'"]/Log/Entry/Distance');
			 for($j=0; $j<$distance->length;$j++){
				 $sum+=($distance->item($j)->textContent);
			 }
			 array_push($dist_15,$sum); 
			 $sum =0; 
		 }
		 
		 //participents of 2016
		  $usNAme16= $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
		 
		 foreach($usNAme16 as $user){
			 array_push($usName16, trim($user->textContent));
		 }
		 
		 //tot dist of each skier 16
		 
		 $sum =0; 
		 for($i=0; $i<sizeof(userName16); $i++){
			 $distance1= $xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = ".$userName16[$i]."]/Log/Entry/Distance');
			 for($j=0; $j<$distance1->length;$j++){
				 $sum+=($distance1->item($j)->textContent);
			 }
			 array_push($dist_16,$sum); 
			 $sum =0; 
		 }
		 // data from 2015 year  in the database
		 $se=$xpath->query('//SkierLogs/Season/@fallYear'); 
		 
		 for($i; $i<sizeof($userNames);$i++){
			 try{
				 $null=NULL; 
				 $cLub= $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = ".$userNames[$i]."]/ancestor::Skiers/@clubId');
				 $input = $this->db->prepare("INSERT INTO distance_skier(userName, fallYear, clubId, total_distance) VALUES(:user, :fallyear, :club, :dist)");
				 $input->bindValue(':user', $userNames[$i], PDO::PARAM_STR);
				 $input->bindValue(':fallyear', $se->item(0)->textContent, PDO::PARAM_STR);
				 $input->bindValue(':dist', $dist_15[$i], PDO::PARAM_STR);
				 if($club->length){
					    $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
					 
				 }
				 else $input->bindValue(':club', $null, PDO::PARAM_STR);
				 $input->execute();
				 }catch(Exception $e){
					 echo "<br>did not manage to get distance db";
					 echo $e->getMessage();
				 }
		 }
		  // data from 2016 year  in the database
		 for($i; $i<sizeof($userSeason16);$i++){
			 try{
				 $null=NULL; 
				 $cLub= $xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = ".$userNames[$i]."]/ancestor::Skiers/@clubId');
				 $input= $this->db->prepare("INSERT INTO distance_skier(userName, fallYear, clubId, total_distance) VALUES(:user, :fallyear, :club, :dist)");
				 $input->bindValue(':user', $userSeason16[$i], PDO::PARAM_STR);
				 $input->bindValue(':fallyear', $se->item(1)->textContent, PDO::PARAM_STR);
				 $input->bindValue(':dist', $dist_15[$i], PDO::PARAM_STR);
				 if($club->length){
					    $input->bindValue(':club', $club->item(0)->textContent, PDO::PARAM_STR);
					 
				 }
				 else $input->bindValue(':club', $null, PDO::PARAM_STR);
				 $input->execute();
				 }catch(Exception $e){
					 echo "<br>did not manage to get distance db";
					 echo $e->getMessage();
				 }
		 }
		 }
	}		 
?>	     